<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$context["can_edit"] = true;

$context["can_edit"] = $context["can_edit"] && get("edit") == "1";

if(EDITABLES_COUNT)
{
	$conn = Db::GetNewConnection();
	for($i = 1; $i <= EDITABLES_COUNT; $i++)
	{
		$key = "editable" . $i;
		$sql = "SELECT `value` FROM `configuration` WHERE `key` = '$key'";
		$data = Db::ExecuteFirst($sql, $conn);
		$context[$key] = $data["value"];
	}
	Db::CloseConnection($conn);
}

echo $twig->render('index.html', $context);

/*$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo 'Page generated in '.$total_time.' seconds.';*/