<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$name = post("name");
	$email = post("email");
	$message = post("message");
	
	$body = "";			
	$body .= "organization name: " . $_POST["business_name"] . "\n";
	$body .= "mailing address: " . $_POST["mailing_address"] . "\n";
	$body .= "physical address: " . $_POST["physical_address"] . "\n";
	$body .= "contact name: " . $_POST["contact_name"] . "\n";
	$body .= "telephone: " . $_POST["telephone"] . "\n";
	$body .= "email: " . $_POST["email"] . "\n";
	$body .= "url: " . $_POST["url"] . "\n";

	$subject = "webinar request";
				
	$sendmail1 = mail("dgurka@enablepoint.com", $subject, $body, "From: webinar@falsealarmsoftware.com");
	$sendmail2 = mail("frank@falsealarmsoftware.com", $subject, $body, "From: webinar@falsealarmsoftware.com");
	$sendmail3 = mail("frankafarren@gmail.com", $subject, $body, "From: webinar@falsealarmsoftware.com");
	$sendmail4 = mail("frank@enablepoint.com", $subject, $body, "From: webinar@falsealarmsoftware.com");
	
	
	//if(mail(ADMIN_EMAIL, "Contact Request", $body, "From: " . NO_REPLY_EMAIL ))
	if($sendmail1 && $sendmail2 && $sendmail3 && $sendmail4)
	{
		$context["message"] = "Thank you for your interest. A webinar representative will be contacting you soon.";
	}
	else
	{
		$context["message"] = "An error has occured, please try again.";
	}
}

echo $twig->render('register.html', $context);