<?php

/**
 * this is the mysqli implementation
 * 
 * @author Joseph Dotson
 */
class Db
{
	public static function GetNewConnection()
	{
		return new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
	}
	public static function CloseConnection($conn)
	{
		$conn->close();
	}

	public static function CheckConnection($conn)
	{
		if($conn != null)
			return true;
		else
			return false;
	}

	public static function EscapeString($string, $conn)
	{
		return $conn->escape_string($string);
	}

	public static function ExecuteQuery($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = array();
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}

		$result->free();

		return $data;
	}
	public static function ExecuteNonQuery($query, $conn)
	{
		$conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);
	}

	public static function ExecuteMultiNonQuery($query, $conn)
	{
		$conn->multi_query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);
	}

	public static function ExecuteFirst($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = null;
		if($row = $result->fetch_assoc())
		{
			$data = $row;
		}

		$result->free();

		return $data;
	}

	public static function GetLastInsertID($conn)
	{
		return $conn->insert_id;
	}
}