<?PHP
//include("includes/global_functs.php");
//include("includes/config.php");

require_once("defines.php");
//require_once(BASE_DIR . "includes/error_handling.php");
require_once(BASE_DIR . "includes/db.php");
//dbconn($db['host'], $db['username'], $db['password'], $db['database']);

function sterilize ($input, $is_sql = false)
{
    $input = htmlentities($input, ENT_QUOTES);

    if(get_magic_quotes_gpc ())
    {
        $input = stripslashes ($input);
    }

    if ($is_sql)
    {
        $input = mysql_real_escape_string ($input);
    }

    $input = strip_tags($input);
    $input = str_replace("
", "\n", $input);

    return $input;
}

if($_POST):
	$zipcode = sterilize($_POST['zipcode']);
	$city = sterilize($_POST['city']);
	$state1 = sterilize($_POST['state1']);
	$state2 = sterilize($_POST['state2']);
	


	if($_POST['mode'] == "citystate" && !$city || $_POST['mode'] == "citystate" && $city == ""){
		$mode = "state";
	} else 	if ($_POST['mode'] == "zip" && !$zipcode || $_POST['mode'] == "zip" && $zipcode == ""){
		header("Location: search.php?error=zip");
	} else {
			$mode = sterilize($_POST['mode']);
	}

// determine mode:
	$addr = "";
	$zoom = 1;
	if($mode == "zip"){
		//$searchq = "SELECT * FROM stores WHERE zip = '".$zipcode."'";
		$searchq = "SELECT street, city, state, zip, phone, storename, food, sell, lat, lng, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance FROM `sasslocator`.`stores` HAVING distance < 25 ORDER BY distance";
		//echo "<h4>search by zip</h4>";
		$addr = $zipcode;
		$searchurl = "search.xml.php?mode=zip&zip=".$zipcode."&state1=null&city=null&zipcode=null&state2=null";
		$zoom = 11;
		//echo $searchurl;
	} else if($mode == "citystate"){
		//$searchq = "SELECT * FROM stores WHERE state LIKE '%".$state1."%' AND city LIKE '%".$city."%'";
		$searchq = "SELECT street, city, state, zip, phone, storename, food, sell, lat, lng, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance FROM `sasslocator`.`stores` HAVING distance < 25 ORDER BY distance";
		//echo "<h4>search by city/state</h4>";
		$addr = "$city, $state1";
		$searchurl = "search.xml.php?mode=citystate&state1=".$state1."&city=".$city."&zipcode=null&state2=null";
		$zoom = 12;
		//echo $searchurl;
	} else if($mode == "state"){
		$searchq = "SELECT * FROM stores WHERE state LIKE '%".$state2."%'";
		//$searchq = "SELECT street, city, state, zip, phone, storename, food, sell, lat, lng, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance FROM `sasslocator`.`stores` WHERE STATE LIKE '%$state%' HAVING distance < 25 ORDER BY distance";
		//echo "<h4>search by state</h4>";
		$addr = $state2;
		$searchurl = "search.xml.php?mode=state&zip=null&state1=null&city=null&zipcode=null&state2=".$state2;
		$zoom = 6;
		//echo $searchurl;
	}
endif;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
var offset = 0;
function init2(){
	var doc = document.getElementById("container_in");
	doc.style.overflow = "scroll";
	var hi = doc.scrollHeight;
	doc.style.overflow = "visible";
	var leftB = document.getElementById("left_b");
	//alert(doc.height);
	leftB.style.height = hi+offset+"px";
	var rightB = document.getElementById("right_b");
	rightB.style.height = hi+offset+"px";
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sweet Sass Foods - Store Lcator</title>

<?php 
	//include("loadfonts.php");
?>

<link href="joes.css" rel="stylesheet" type="text/css" />
<link href="search_style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="./templates/SweetSass/css/main.css" />
    
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<?PHP /*if($_GET['id']){
			getkeywords($_GET['id']);
		}  else { getkeywords("1"); } 

include("includes/googanalytics.php"); */ ?>


<script type="text/javascript">
function init() {
  /*var plotter = new GPlotter();
  plotter.setColor(plotter.BLUE);
  plotter.setIconUrl("http://gplotter.offwhite.net/maps/icons3/");
  plotter.plot("map", "labels", "<?php echo $searchurl; ?>");*/
 
 
}
</script>
<?php 

if($_POST){
$coder = new GeoCoder();
$coding = $coder->getGeoCoding($addr);

?>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script type="text/javascript">
	var lastOpen = null;

  function initialize() {
    var latlng = new google.maps.LatLng(<?php echo $coding->lat ?>, <?php echo $coding->lng ?>);
    var myOptions = {
      zoom: <?php echo $zoom; ?>,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        myOptions);
        
    <?php
    	// Don't ever do this, put the whole thing in a .js.php file unless you are awesome like me
    	require ("getgeocoding.php"); 
    ?>
}
</script>
<?php  } ?>

<body onload="initialize();">

<div class="container" id="contaner">

<div class="header" id="header">
<div id="banner"><a href="http://sweetsassfoods.com/" >
<img id="fox" src="./templates/SweetSass/images/fox_header.jpg"  /></a>

<a href="http://sweetsassfoods.com/" ><img id="logo" src="./templates/SweetSass/images/logo.jpg"  /></a>

<a href="http://sweetsassfoods.com/page/29/" ><img id="bottles" src="./templates/SweetSass/images/bottles.jpg"  /></a>


</a><br clear="all" /></div>

<div class="navigation menu">
    <ul><li class='menu_0'><a href='/page/1/'>Home</a></li><li class='menu_1'><a href='/page/24/'>Products</a><ul><li><a href='/page/25/'>Sassipes</a></ul></li><li class='menu_2'><a href='/page/35/'>Media</a><ul><li><a href='/page/30/'>Fundraising</a><li><a href='/page/26/'>History</a></ul></li><li class='menu_3'><a href='/page/29/'>Order</a></li><li class='menu_4'><a href='/contact'>Contact Us</a><ul><li><a href='search.php'>Store Locator</a></ul></li></ul>
        
    <a style="float:left;margin:10px;" href="/page/29/" >Order Now</a>
    
    <img class="social" id="social" src="./templates/SweetSass/images/social.jpg" width="154" height="43" border="0" usemap="#Map" />
  <map name="Map" id="Map">
    <area shape="rect" coords="5,-7,48,45" href="https://twitter.com/SweetSassFoods" target="_blank" />
    <area shape="rect" coords="58,-14,98,46" href="https://www.facebook.com/pages/Sweet-Sass-Flavor-Sauce/93599279161" target="_blank" />
    <area shape="rect" coords="113,3,151,59" href="http://www.sassipe.com" target="_blank" />
  </map>
  
  
</div>
</div>



<div class="spacer" id="spacer"></div>
<div class="content" id="content">