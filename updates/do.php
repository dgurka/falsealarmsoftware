<?php

require_once("../defines.php");
require_once(BASE_DIR . "includes/error_handling.php");
require_once(BASE_DIR . "includes/db.php");
require_once(BASE_DIR . "includes/functions.php");

$conn = Db::GetNewConnection();

$version_id = 0;
if(count(Db::ExecuteQuery("SHOW TABLES LIKE 'configuration'", $conn)))
{
	// table exists get actual version ID
	$version_id = Db::ExecuteFirst("SELECT `value` FROM `configuration` WHERE `key` = 'version_id'", $conn);
	$version_id = (int)$version_id["value"];
}

Db::CloseConnection($conn);

if(APP_VERSION_ID > $version_id)
{
	$_apv = APP_VERSION_ID;
	$_apc = APP_VERSION_CODE;
	echo "updating to version $_apv ($_apc)...<br/>";
	flush(); // flush the text so the user knows what's up
	for ($i=$version_id+1; $i <= $_apv; $i++) 
	{ 
		$conn = Db::GetNewConnection(); // get new connection

		echo "installing version $i...";
		flush();
		$path = BASE_DIR . "/updates/" . $i . ".php";
		$path_sql = BASE_DIR . "/updates/" . $i . ".sql";
		if(file_exists($path))
		{
			require_once($path);
			$fname = "update" . $i;
			$fname($conn); // run the function passing the connection
		}
		else if(file_exists($path_sql))
		{
			$sql = file_get_contents($path_sql);
			Db::ExecuteMultiNonQuery($sql, $conn);
		}
		echo " done<br/>";
		flush();

		Db::CloseConnection($conn);
	}
	$conn = Db::GetNewConnection(); // get new connection
	Db::ExecuteNonQuery("UPDATE `configuration` SET `value` = '$_apv' WHERE `key` = 'version_id'", $conn);
	echo "updates complete";
	Db::CloseConnection($conn);
}
else
{
	echo "no updates";
}