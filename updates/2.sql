ALTER TABLE `event` 
  ADD COLUMN `submitter_name` VARCHAR(255) NULL  AFTER `ID` , 
  ADD COLUMN `submitter_phone` VARCHAR(45) NULL  AFTER `submitter_name` , 
  ADD COLUMN `submitter_email` VARCHAR(255) NULL  AFTER `submitter_phone` , 
  ADD COLUMN `event_title` VARCHAR(255) NULL  AFTER `submitter_email` , 
  ADD COLUMN `caption` VARCHAR(255) NULL  AFTER `event_title` , 
  ADD COLUMN `start_date` DATE NULL  AFTER `caption` , 
  ADD COLUMN `start_time` TIME NULL  AFTER `start_date` , 
  ADD COLUMN `end_date` DATE NULL  AFTER `start_time` , 
  ADD COLUMN `end_time` TIME NULL  AFTER `end_date` , 
  ADD COLUMN `type` INT NULL  AFTER `end_time` , 
  ADD COLUMN `location` VARCHAR(255) NULL  AFTER `type` , 
  ADD COLUMN `location_address` VARCHAR(255) NULL  AFTER `location` , 
  ADD COLUMN `description` TEXT NULL  AFTER `location_address` , 
  ADD COLUMN `img_loc` VARCHAR(255) NULL  AFTER `description` ,
  ADD COLUMN `pending` TINYINT(1) NULL  AFTER `img_loc` ,
  ADD COLUMN `featured` TINYINT(1) NULL  AFTER `pending` ;

CREATE  TABLE `directory_cat` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  PRIMARY KEY (`ID`) );

CREATE  TABLE `directory_sub_cat` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `parent` INT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  PRIMARY KEY (`ID`) );

CREATE  TABLE `directory` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `parent` INT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `address` VARCHAR(255) NULL ,
  `address_2` VARCHAR(255) NULL ,
  `city` VARCHAR(255) NULL ,
  `state` VARCHAR(10) NULL ,
  `zip` VARCHAR(10) NULL ,
  `phone` VARCHAR(45) NULL ,
  `fax` VARCHAR(45) NULL ,
  `email` VARCHAR(255) NULL ,
  `contact_person` VARCHAR(255) NULL ,
  `contact_phone` VARCHAR(45) NULL ,
  `website` VARCHAR(255) NULL ,
  `hours` VARCHAR(255) NULL ,
  `img_loc` VARCHAR(255) NULL ,
  `description` TEXT NULL ,
  `lat` VARCHAR(45) NULL ,
  `long` VARCHAR(45) NULL ,
  PRIMARY KEY (`ID`) );

CREATE  TABLE `cache` (
  `key` VARCHAR(255) NOT NULL ,
  `value` TEXT NULL ,
  PRIMARY KEY (`key`) );
