<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$submitter_name = Db::EscapeString(post("submitter_name"), $conn);
	$submitter_phone = Db::EscapeString(post("submitter_phone"), $conn);
	$submitter_email = Db::EscapeString(post("submitter_email"), $conn);
	$event_title = Db::EscapeString(post("event_title"), $conn);
	$caption = Db::EscapeString(post("caption"), $conn);
	$start_date = dbDate(post("start_date"));
	$start_time = dbTime(post("start_time"), $conn);
	$end_date = dbDate(post("end_date"), $conn);
	$end_time = dbTime(post("end_time"), $conn);
	$typekey = Db::EscapeString(post("typekey"), $conn);
	$location = Db::EscapeString(post("location"), $conn);
	$location_address = Db::EscapeString(post("location_address"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$pending = (int)(post("pending") == "yes");
	$featured = (int)(post("featured") == "yes");

	$img_loc = saveFile("image", "event_images");
	
	if($img_loc !== false)
		$img_loc = " , img_loc = '$img_loc'";
	else
		$img_loc = "";

	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE event SET submitter_name = '$submitter_name', submitter_phone = '$submitter_phone', submitter_email = '$submitter_email', event_title = '$event_title', caption = '$caption', start_date = $start_date, start_time = $start_time, end_date = $end_date, end_time = $end_time, `type` = '$typekey', location = '$location', location_address = '$location_address', description = '$description', pending = '$pending', featured = '$featured' $img_loc WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/event/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO event SET submitter_name = '$submitter_name', submitter_phone = '$submitter_phone', submitter_email = '$submitter_email', event_title = '$event_title', caption = '$caption', start_date = $start_date, start_time = $start_time, end_date = $end_date, end_time = $end_time, `type` = '$typekey', location = '$location', location_address = '$location_address', description = '$description', pending = '$pending', featured = '$featured' $img_loc";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/event/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$event = Db::ExecuteFirst("SELECT * FROM event WHERE ID = $id", $conn);
	$submitter_name = q($event["submitter_name"]);
	$submitter_phone = q($event["submitter_phone"]);
	$submitter_email = q($event["submitter_email"]);
	$event_title = q($event["event_title"]);
	$caption = q($event["caption"]);
	$start_date = disDate($event["start_date"]);
	$start_time = disTime($event["start_time"]);
	$end_date = disDate($event["end_date"]);
	$end_time = disTime($event["end_time"]);
	$typekey = $event["type"];
	$location = q($event["location"]);
	$location_address = $event["location_address"];
	$description = $event["description"];
	$img_loc = $event["img_loc"];
	$pending = (int)$event["pending"];
	$featured = (int)$event["featured"];
}
else
{
	$submitter_name = "";
	$submitter_phone = "";
	$submitter_email = "";
	$event_title = "";
	$caption = "";
	$start_date = "";
	$start_time = "";
	$end_date = "";
	$end_time = "";
	$typekey = -1;
	$location = "";
	$location_address = "";
	$description = "";
	$img_loc = "";
	$pending = 0;
	$featured = 0;
}

if($pending)
	$pending = "checked='checked'";
else
	$pending = "";

if($featured)
	$featured = "checked='checked'";
else
	$featured = "";

$typeselect = array();
$typeselect[] = "<option value='-1'>" . GENERAL_EVENT_NAME . "</option>";
$typeitems = explode(",", EVENT_CATEGORIES);
foreach ($typeitems as $i => $value) 
{
	$m = "<option value=\"$i\"";
	if($i == $typekey)
		$m .= " selected='selected'";
	$m .= ">$value</option>";
	$typeselect[] = $m;
}

$context["submitter_name"] = $submitter_name;
$context["submitter_phone"] = $submitter_phone;
$context["submitter_email"] = $submitter_email;
$context["event_title"] = $event_title;
$context["caption"] = $caption;
$context["start_date"] = $start_date;
$context["start_time"] = $start_time;
$context["end_date"] = $end_date;
$context["end_time"] = $end_time;
$context["types"] = implode("", $typeselect);
$context["location"] = $location;
$context["location_address"] = $location_address;
$context["description"] = $description;
$context["img_loc"] = $img_loc;
$context["pending"] = $pending;
$context["featured"] = $featured;

Db::CloseConnection($conn);

echo $twig->render('event.html', $context);