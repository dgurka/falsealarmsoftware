<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$i = 1;

while(isset($_POST["editable" . $i]))
{
	$key = "editable" . $i;
	$data = post($key);

	$conn = Db::GetNewConnection();

	$data = Db::EscapeString($data, $conn);

	$sql = "UPDATE `configuration` SET `value` = '$data' WHERE `key` = '$key'";
	Db::ExecuteNonQuery($sql, $conn);

	Db::CloseConnection($conn);

	$i++;
}

redirect(URL_ROOT . "?edit=1");